# Discuz! 官方插件 发帖际遇

## 维护说明

**本插件自 2.5 版本开始由 Discuz! 应用中心团队与 Discuz! X 社区开发者团队共同维护，您可以在我们的 Gitee 仓库 https://gitee.com/discuzaddons/luckypost/ 上向我们反馈程序 Bug 。**

## 插件说明

发帖时会根据后台设定的概率产生特定际遇，对发帖者进行积分的奖惩，增加用户发帖的趣味性。

## 事件设置

您可以根据您的想法设置发帖碰到的事件，并基于此事件对发帖者进行积分的奖惩。

举例：

`2|1,5|一袋金币砸在了 {username} 头上，{username} 赚了 {credit} {extcredits}.|小财神卡|g_1s.jpg`

以上例子中参数以(|)分隔从左到右解释如下：

第 1 个参数中的 2 为积分类型 ID ，表示 `extcredits2` ，在默认程序里面就是金钱。

第 2 个参数中的 1,5 为奖励或惩罚的积分的最小值与最大值，以 `,` 隔开。

第 3 个参数为事件详情，用户名用 `{username}` 表示 ，积分数量用 `{credit}` 表示， 积分名用 `{extcredits}` 表示。

（以下为非必要参数，如果有以下参数则会变成卡片模式）

第 4 个参数为卡片名称。

第 5 个参数为卡片图片文件名，图片文件在 /source/plugin/luckypost/template/image/ 下面。

第 4 个和第 5 个参数作用效果请参考：https://static.dismall.com/202103130655/48c99d071e1bedc07693a0dc8527598f/resource/preview/2/3.jpg.thumb.jpg

## 颜色设置

可以随意的设置发帖碰到际遇事件在帖子显示的颜色(卡片模式为卡片的背景色)，插件默认设置为黑色。

## 关于论坛本身的伪静态/导航链接不存在问题

导航链接显示为 luckypost-show.html，说明您的论坛开启了插件伪静态，您需要到服务器上配置相应的伪静态规则才可以正常使用。

请参考后台 全局 » SEO设置 » URL 静态化 » 查看当前的 Rewrite 规则 页面的信息进行修改，或者在 全局 » SEO设置 » URL 静态化 页面关闭插件伪静态规则。